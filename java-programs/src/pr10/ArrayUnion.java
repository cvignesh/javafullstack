package pr10;

import java.util.ArrayList;
import java.util.Arrays;

public class ArrayUnion {

	public static void main(String[] args) {
		Integer[] a = { 1, 2, 3, 4 };
		Integer[] b = { 3, 4, 5, 6 };
		ArrayList<Integer> unionArr = new ArrayList<Integer>(Arrays.asList(a));
		for (int num : b) {
			int c = Arrays.asList(a).indexOf(num);
			if (c == -1) {
				unionArr.add(num);
			}
		}
		System.out.println(unionArr);
		// Convert arraylist to array
	}

}
