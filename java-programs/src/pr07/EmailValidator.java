package pr07;

public class EmailValidator {

	public static void main(String[] args) {
		String input = "test@gmail.com";
		boolean output = checkSpecialChar(input) & checkEndsWithcom(input) & checkSpecialCharRepeation(input) & checkminLen(input);
		System.out.println(output);
	}

	// check if '@' and '.' are present
	private static boolean checkSpecialChar(String str) {
		boolean specalChars = false;
		if (str.indexOf('.') != -1 && str.indexOf('@') != -1) {
			specalChars = true;
		}
		System.out.println(specalChars);
		return specalChars;
	}

	// check if '@' and ` . ` and '&' are unique
	private static boolean checkSpecialCharRepeation(String str) {
		boolean isCharUnique = false;
		;
		int count_at = 0, count_dot = 0;
		for (char ch : str.toCharArray()) {
			if (ch == '@') {
				count_at++;
			}
			if (ch == '.') {
				count_dot++;
			}
		}
		if (count_at == 1 && count_dot == 1) {
			isCharUnique = true;
		}
		System.out.println(isCharUnique);
		return isCharUnique;
	}

	private static boolean checkminLen(String str1) {
		boolean ispreLen = false, ispostlen = false, isValid = false;
		String string = str1.toString();
		String[] parts = string.split("@");

		if (parts.length == 2) {
			String part1 = parts[0].toString();
			String part2 = parts[1].toString();
			if (part1.length() >= 3) {
				ispreLen = true;
			}
			String[] subStrpart = part2.split("\\.");
			String subStrPrePart = subStrpart.toString();
			if (subStrPrePart.length() >= 4) {
				ispostlen = true;
			}
		}
		isValid = ispreLen & ispostlen;
		System.out.println(isValid);
		return isValid;
	}

	private static boolean checkEndsWithcom(String str1) {
		String string = str1.toString();
		boolean isValid = string.endsWith(".com");
		System.out.println(isValid);
		return isValid;
	}

}
