package pr15;

public class UniqueChar {
	public static void main(String[] args) {
		String input = "helloworld";
		String output = "";
		for (char c : input.toCharArray()) {
			if (output.indexOf(c) == -1) {
				output += c;
			}
		}
		System.out.println(output);
	}
}
