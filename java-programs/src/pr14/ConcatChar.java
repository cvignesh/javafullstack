package pr14;

public class ConcatChar {

	public static void main(String[] args) {
		String[] input1 = { "abc", "da", "ram" };
		int input2 = 3;
		String output = "";
		for (String strinput : input1) {
			int strlen = strinput.length();
			if (input2 > strlen) {
				output += '$';
			} else {
				output += strinput.charAt(input2 - 1);
			}
		}
		System.out.println(output);
	}

}
