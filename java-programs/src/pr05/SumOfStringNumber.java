package pr05;

public class SumOfStringNumber {

	public static void main(String[] args) {
		String[] strArr = { "2AA", "12", " ABC", "c1a" };
		String num = "1234567890";
		int sum = 0;
		for (String str : strArr) {
			for (char c : str.toCharArray()) {
				if (num.indexOf(c) > -1) {
					int number = Integer.parseInt(String.valueOf(c));
					sum += number;
				}
			}
		}
		System.out.println(sum);
	}

}
