package pr04;

public class WordCount {

	public static void main(String[] args) {
		getStringCount(new String[] { "aa", "b", "cc", "ddd" }, 2);

		getStringCount(new String[] { "aa", "b", "cc", "ddd" }, 3);
	}

	private static void getStringCount(String[] input1, int len) {

		int count = 0;
		for (String str : input1) {
			if (str.length() == len)
				count++;
		}
		System.out.println("No of string with length :" + len + " :"  + count);
	}

}
